# Rclone

## Run interactively

```shell
AWS_ACCESS_KEY=<set-as-appropriate>
AWS_SECRET_KEY=<set-as-appropriate>

docker run --rm -it -e AWS_ACCESS_KEY_ID=$AWS_ACCESS_KEY \
                -e AWS_SECRET_ACCESS_KEY=$AWS_SECRET_KEY \
                -e RCLONE_CONFIG_S3_TYPE=s3 \
                -e RCLONE_CONFIG_S3_PROVIDER=ceph \
                -e RCLONE_CONFIG_S3_ENV_AUTH=true \
                -e RCLONE_CONFIG_S3_ENDPOINT=https://cs3.cern.ch \
                -e RCLONE_CONFIG_S3_ACL=private \
                --entrypoint /bin/sh \
                gitlab-registry.cern.ch/vcs/rclone-docker
```

FROM alpine:latest

ARG RCLONE_DOWNLOAD_URL="https://downloads.rclone.org/rclone-current-linux-amd64.zip"

ENV CERN_CA_BUNDLE="/usr/local/share/ca-certificates/cern-bundle.pem"

RUN \
 apk update && \
 apk add --no-cache ca-certificates openssl wget curl unzip \
 # To install awscli
 py-pip gcc groff less && \
 pip install awscli --upgrade && \
 cd /tmp && \
 wget ${RCLONE_DOWNLOAD_URL} && \
 unzip /tmp/rclone-*.zip && \
 mv /tmp/rclone-*/rclone /usr/bin && \
 # cleanup
 rm -rf /tmp/* /var/tmp/* /var/cache/apk/*

# Install CERN CA to authenticate to backup server
RUN curl -o /usr/local/share/ca-certificates/cern-root.crt https://cafiles.cern.ch/cafiles/certificates/CERN%20Root%20Certification%20Authority%202.crt && \
    openssl x509 -inform DER -in /usr/local/share/ca-certificates/cern-root.crt -out /usr/local/share/ca-certificates/cern-root.pem -outform PEM && \
    rm /usr/local/share/ca-certificates/cern-root.crt && \
    curl -o /usr/local/share/ca-certificates/cern-grid.pem https://cafiles.cern.ch/cafiles/certificates/CERN%20Grid%20Certification%20Authority.crt && \
    cat /usr/local/share/ca-certificates/cern-root.pem /usr/local/share/ca-certificates/cern-grid.pem > $CERN_CA_BUNDLE && \
    update-ca-certificates

VOLUME ["/data"]

ENTRYPOINT ["/usr/bin/rclone"]
